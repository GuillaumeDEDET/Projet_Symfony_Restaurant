Restaurant_Symfony
==================

A Symfony project created on January 31, 2017, 1:09 pm.

Pour vérifier les menus sans plats : app:empty-menu

Pour créer des utilisateurs de chaque rôle sur l'appli url : /loadUsers

La fixture qui créé des utilisateurs leur assigne des rôles 
 (Chef, Reviewer, Editeur, Serveur).
 
 L'identifiant de chaque utilisateur est nom_du_role (En minuscule) et le mot de passe pour tout le monde est admin

