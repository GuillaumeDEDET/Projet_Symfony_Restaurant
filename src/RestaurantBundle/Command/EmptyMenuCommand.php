<?php

namespace RestaurantBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Created by PhpStorm.
 * User: quentin
 * Date: 15/02/2017
 * Time: 14:31
 */
class EmptyMenuCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:empty-menu')
             ->setDescription('Envoie un email aux administrateurs pour chaque menu ne possèdant pas de plats');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $menus = $em->getRepository('RestaurantBundle:Menu')->findNoPlats();
        $listChefs = $em->getRepository('RestaurantBundle:User')->findByRole('ROLE_CHEF');

        foreach ($listChefs as $chef) {
                foreach ($menus as $menu) {
                    $message = \Swift_Message::newInstance()
                        ->setSubject('Ce menu n\'a pas de plats')
                        ->setFrom('restaurantcodeacademy@gmail.com')
                        ->setTo($chef->getEmail())
                        ->setBody(
                            $this->getContainer()->get('templating')->render(
                                'RestaurantBundle:Emails:noplats-menu.html.twig',
                                array('firstname' => $chef->getFirstname(),
                                    'lastname' => $chef->getLastname(),
                                    'menu' => $menu)
                            ),
                            'text/html'
                        );
                    $this->getContainer()->get('mailer')->send($message);
                    $output->writeln("Mail Envoyé");
                }
        }
    }
}