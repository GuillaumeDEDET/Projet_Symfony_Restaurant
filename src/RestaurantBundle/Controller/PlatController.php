<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\Plat;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Plat controller.
 *
 * @Route("plat")
 */
class PlatController extends Controller
{

    /**
     * Lists all plat entities.
     *
     * @Route("/index/{page}", name="plat_index", requirements={"page" = "\d+"},defaults={"page" = 1})
     * @Method("GET")
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();

        $plats = $em->getRepository('RestaurantBundle:Plat')->findAccepted($page,Plat::MAX_PER_PAGE);

        $pagination = array(
            'page' => $page,
            'route' => 'plat_index',
            'pages_count' => ceil(count($plats) / Plat::MAX_PER_PAGE),
            'route_params' => array()
        );

        return $this->render('RestaurantBundle:plat:index.html.twig', array(
            'plats' => $plats,
            'pagination' => $pagination
        ));
    }


    /**
     * Show the admin index for plats
     *
     * @Route("/admin_index/{page}", name="plat_admin_index", requirements={"page" = "\d+"},defaults={"page" = 1})
     * @Security("has_role('ROLE_EDITEUR')")
     * @Method({"GET", "POST"})
     */
    public function adminIndexAction(Request $request, $page){

        $em = $this->getDoctrine()->getManager();
        $form= $this->createFormFilters();
        $form->handleRequest($request);
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $isReviewer= $this->container->get('security.authorization_checker')->isGranted('ROLE_REVIEWER');

        if($request->getMethod() == 'POST') {
            if ($form->isSubmitted() && $form->isValid()) {
                $choix = $form->getData()['filtre'];
                $plats = $em->getRepository('RestaurantBundle:Plat')->filterPlats($user->getId(),$choix,$isReviewer, $page, Plat::MAX_PER_PAGE);
                $pagination = array(
                    'page' => $page,
                    'route' => 'plat_admin_index',
                    'pages_count' => ceil(count($plats) / Plat::MAX_PER_PAGE),
                    'route_params' => array('choix' =>$choix)
                );

                return $this->render('RestaurantBundle:plat:admin_index.html.twig', array(
                    'plats' => $plats,
                    'filterForm' => $form->createView(),
                    'pagination' => $pagination
                ));
            }
        }

        $plats = $em->getRepository('RestaurantBundle:Plat')->filterPlats($user->getId(),'PENDING',$isReviewer, $page, Plat::MAX_PER_PAGE);

        $pagination = array(
            'page' => $page,
            'route' => 'plat_admin_index',
            'pages_count' => ceil(count($plats) / Plat::MAX_PER_PAGE),
            'route_params' => array('choix' =>'PENDING')
        );


        return $this->render('RestaurantBundle:plat:admin_index.html.twig', array(
            'plats' => $plats,
            'filterForm' =>$form->createView(),
            'pagination' => $pagination
        ));

    }


    /**
     * Validate a Plat
     *
     * @param Plat $plat
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/accept/{id}", name="plat_accept")
     * @Security("has_role('ROLE_REVIEWER')")
     * @Method("GET")
     */
    public function acceptPlatAction(Plat $plat){
        $plat->setStatus("ACCEPTED");
        $this->getDoctrine()->getManager()->flush();
        $this->sendMailToServeurs($plat);
        return $this->redirectToRoute('plat_admin_index');
    }


    /**
     * Refuse a plat
     *
     * @param Plat $plat
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/refuse/{id}", name="plat_refuse")
     * @Security("has_role('ROLE_REVIEWER')")
     * @Method("GET")
     */
    public function refusePlatAction(Plat $plat){
        $plat->setStatus("REFUSED");
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('plat_admin_index');
    }


    /**
     * Creates a new plat entity.
     *
     * @Route("/new", name="plat_new")
     * @Security("has_role('ROLE_EDITEUR')")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $plat = new Plat();
        $form = $this->createForm('RestaurantBundle\Form\PlatType', $plat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $action = $form->get('saveAsDraft')->getData() ? 'DRAFT' : 'PENDING';
            $plat->setStatus($action);
            $em = $this->getDoctrine()->getManager();
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $plat->setAuthor($user);
            $em->persist($plat);
            $em->flush($plat);

            if($plat->getStatus() == 'PENDING')
            {
                $this->sendMailToChefAndReviewerAdd($plat);
            }

            return $this->redirectToRoute('plat_show', array('id' => $plat->getId()));
        }

        return $this->render('RestaurantBundle:plat:new.html.twig', array(
            'plat' => $plat,
            'form' => $form->createView(),
        ));
    }


    /**
     * Finds and displays a plat entity.
     *
     * @Route("/{id}", name="plat_show")
     * @Method("GET")
     */
    public function showAction(Plat $plat)
    {
        $deleteForm = $this->createDeleteForm($plat);

        return $this->render('RestaurantBundle:plat:show.html.twig', array(
            'plat' => $plat,
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing plat entity.
     *
     * @Route("/{id}/edit", name="plat_edit")
     * @Security("has_role('ROLE_EDITEUR')")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Plat $plat)
    {
        $deleteForm = $this->createDeleteForm($plat);

        $imageBefore = $plat->getImage() ? $plat->getImage() : null;

        $editForm = $this->createForm('RestaurantBundle\Form\PlatType', $plat);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $action = $editForm->get('saveAsDraft')->getData() ? 'DRAFT' : 'PENDING';

            if($editForm->get('image')->getData() === null && $imageBefore !== null) $plat->setImage($imageBefore);

            $plat->setStatus($action);
            $this->getDoctrine()->getManager()->flush();

            if($plat->getStatus() == 'PENDING')
            {
                $this->sendMailToChefAndReviewerEdit($plat);
            }

            return $this->redirectToRoute('plat_show', array('id' => $plat->getId()));
        }

        return $this->render('RestaurantBundle:plat:edit.html.twig', array(
            'plat' => $plat,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Deletes a plat entity.
     *
     * @Route("/delete/{id}", name="plat_delete")
     * @Method("GET")
     * @Security("has_role('ROLE_CHEF')")
     */
    public function deleteAction(Plat $plat)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($plat);
        $em->flush($plat);

        return $this->redirectToRoute('plat_admin_index');
    }


    /**
     * Creates a form to delete a plat entity.
     *
     * @param Plat $plat The plat entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Plat $plat)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('plat_delete', array('id' => $plat->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    /**
     * Create a form to filter plats
     * @return mixed
     */
    private function createFormFilters(){

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('plat_admin_index'))
            ->setMethod('POST')
            ->add('filtre',ChoiceType::class, array(
                'choices' => array(
                    'En Attente' => 'PENDING',
                    'Refusé'     => 'REFUSED',
                    'Validé' => 'ACCEPTED',
                    'Brouillon' => 'DRAFT'
                )
            ) )
            ->add('Filter', SubmitType::class, array('label' => 'Filtrer'))
            ->getForm();
    }

    // --------------  MAILS METHOD ----------------- \\

    /**
     * Send Mail To Serveurs
     * @param $plat
     */
    private function sendMailToServeurs($plat)
    {
        $em = $this->getDoctrine()->getManager();

        $listServeur = $em->getRepository('RestaurantBundle:User')->findByRole('ROLE_SERVEUR');

        foreach ($listServeur as $serveur) {
            $message = \Swift_Message::newInstance()
                ->setSubject('Un plat a été mis en ligne')
                ->setFrom('restaurantcodeacademy@gmail.com')
                ->setTo($serveur->getEmail())
                ->setBody(
                    $this->renderView(
                        'RestaurantBundle:Emails:accepted-plat.html.twig',
                        array('firstname' => $serveur->getFirstname(),
                            'lastname' => $serveur->getLastname(),
                            'plat' => $plat)
                    ),
                    'text/html'
                )
            ;
            $this->get('mailer')->send($message);
        }
    }


    /**
     * Send Mail To Chef And Reviewer for added Plat
     * @param $plat
     */
    private function sendMailToChefAndReviewerAdd($plat)
    {
        $em = $this->getDoctrine()->getManager();
        $listChef = $em->getRepository('RestaurantBundle:User')->findByRole('ROLE_CHEF');
        $listReviewer = $em->getRepository('RestaurantBundle:User')->findByRole('ROLE_REVIEWER');
        $listReceivers = array_merge($listChef, $listReviewer);

        foreach ($listReceivers as $receiver) {
            $message = \Swift_Message::newInstance()
                ->setSubject('Un plat a été créé')
                ->setFrom('restaurantcodeacademy@gmail.com')
                ->setTo($receiver->getEmail())
                ->setBody(
                    $this->renderView(
                        'RestaurantBundle:Emails:created-plat.html.twig',
                        array('firstname' => $receiver->getFirstname(),
                            'lastname' => $receiver->getLastname(),
                            'plat' => $plat)
                    ),
                    'text/html'
                )
            ;
            $this->get('mailer')->send($message);
        }
    }


    /**
     * Send Mail To Chef And Reviewer for edited Plat
     * @param $plat
     */
    private function sendMailToChefAndReviewerEdit($plat)
    {
        $em = $this->getDoctrine()->getManager();
        $listChef = $em->getRepository('RestaurantBundle:User')->findByRole('ROLE_CHEF');
        $listReviewer = $em->getRepository('RestaurantBundle:User')->findByRole('ROLE_REVIEWER');
        $listReceivers = array_merge($listChef, $listReviewer);

        foreach ($listReceivers as $receiver) {
            $message = \Swift_Message::newInstance()
                ->setSubject('Un plat a été modifié')
                ->setFrom('restaurantcodeacademy@gmail.com')
                ->setTo($receiver->getEmail())
                ->setBody(
                    $this->renderView(
                        'RestaurantBundle:Emails:edited-plat.html.twig',
                        array('firstname' => $receiver->getFirstname(),
                            'lastname' => $receiver->getLastname(),
                            'plat' => $plat)
                    ),
                    'text/html'
                )
            ;
            $this->get('mailer')->send($message);
        }
    }

    // --------------  / MAILS METHOD ----------------- \\
}
