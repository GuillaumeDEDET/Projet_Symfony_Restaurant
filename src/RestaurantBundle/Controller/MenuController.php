<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\Menu;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Menu controller.
 *
 * @Route("menu")
 */
class MenuController extends Controller
{

    /**
     * Lists all menu entities.
     *
     * @Route("/index/{page}", name="menu_index", requirements={"page" = "\d+"},defaults={"page" = 1})
     * @Method("GET")
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();

        $menus = $em->getRepository('RestaurantBundle:Menu')->findAccepted($page, Menu::MAX_PER_PAGE);

        $pagination = array(
            'page' => $page,
            'route' => 'menu_index',
            'pages_count' => ceil(count($menus) / Menu::MAX_PER_PAGE),
            'route_params' => array()
        );

        return $this->render('RestaurantBundle:menu:index.html.twig', array(
            'menus' => $menus,
            'pagination' => $pagination
        ));
    }


    /**
     * Show the admin index for menus
     *
     * @Route("/admin_index/{page}", name="menu_admin_index", requirements={"page" = "\d+"},defaults={"page" = 1})
     * @Security("has_role('ROLE_EDITEUR')")
     * @Method({"GET", "POST"})
     */
    public function adminIndexAction(Request $request, $page){


        $em = $this->getDoctrine()->getManager();
        $form= $this->createFormFilters();
        $form->handleRequest($request);
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $isReviewer= $this->container->get('security.authorization_checker')->isGranted('ROLE_REVIEWER');
        if($request->getMethod() == 'POST') {
            if ($form->isSubmitted() && $form->isValid()) {
                $choix = $form->getData()['filtre'];
                $menus = $em->getRepository('RestaurantBundle:Menu')->filterMenus($user->getId(),$choix,$isReviewer, $page, Menu::MAX_PER_PAGE);

                $pagination = array(
                    'page' => $page,
                    'route' => 'menu_admin_index',
                    'pages_count' => ceil(count($menus) / Menu::MAX_PER_PAGE),
                    'route_params' => array('choix' =>$choix)
                );

                return $this->render('RestaurantBundle:menu:admin_index.html.twig', array(
                    'menus' => $menus,
                    'filterForm' => $form->createView(),
                    'pagination' => $pagination
                ));
            }
        }

        $menus = $em->getRepository('RestaurantBundle:Menu')->filterMenus($user->getId(),'PENDING',$isReviewer, $page, Menu::MAX_PER_PAGE);

        $pagination = array(
            'page' => $page,
            'route' => 'menu_admin_index',
            'pages_count' => ceil(count($menus) / Menu::MAX_PER_PAGE),
            'route_params' => array('choix' =>'PENDING')
        );

        return $this->render('RestaurantBundle:menu:admin_index.html.twig', array(
            'menus' => $menus,
            'filterForm' =>$form->createView(),
            'pagination' => $pagination
        ));

    }


    /**
     * Validate a menu
     *
     * @param Menu $menu
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/accept/{id}", name="menu_accept")
     * @Security("has_role('ROLE_REVIEWER')")
     * @Method("GET")
     */
    public function acceptMenuAction(Menu $menu){
        $menu->setStatus("ACCEPTED");

        $this->sendMailToServeurs($menu);

        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('menu_admin_index');
    }


    /**
     * Refuse a menu
     *
     * @param Menu $menu
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/refuse/{id}", name="menu_refuse")
     * @Security("has_role('ROLE_REVIEWER')")
     * @Method("GET")
     */
    public function refuseMenuAction(Menu $menu){
        $menu->setStatus("REFUSED");
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('menu_admin_index');
    }


    /**
     * Creates a new menu entity.
     * @Security("has_role('ROLE_EDITEUR')")
     * @Route("/new", name="menu_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $menu = new Menu();
        $form = $this->createForm('RestaurantBundle\Form\MenuType', $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $action = $form->get('saveAsDraft')->getData() ? 'DRAFT' : 'PENDING';
            $menu->setStatus($action);
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $menu->setAuthor($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($menu);
            $em->flush($menu);

            if($menu->getStatus() == 'PENDING') {
                $this->sendMailToChefAndReviewerAdd($menu);
            }

            return $this->redirectToRoute('menu_show', array('id' => $menu->getId()));
        }

        return $this->render('RestaurantBundle:menu:new.html.twig', array(
            'menu' => $menu,
            'form' => $form->createView(),
        ));
    }


    /**
     * Finds and displays a menu entity.
     *
     * @Route("/{id}", name="menu_show")
     * @Method("GET")
     */
    public function showAction(Menu $menu)
    {
        $deleteForm = $this->createDeleteForm($menu);

        return $this->render('RestaurantBundle:menu:show.html.twig', array(
            'menu' => $menu,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing menu entity.
     * @Security("has_role('ROLE_EDITEUR')")
     * @Route("/{id}/edit", name="menu_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Menu $menu)
    {
        $deleteForm = $this->createDeleteForm($menu);
        $editForm = $this->createForm('RestaurantBundle\Form\MenuType', $menu);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $action = $editForm->get('saveAsDraft')->getData() ? 'DRAFT' : 'PENDING';
            $menu->setStatus($action);
            $this->getDoctrine()->getManager()->flush();

            if($menu->getStatus() == 'PENDING') {
                $this->sendMailToChefAndReviewerEdit($menu);
            }

            return $this->redirectToRoute('menu_show', array('id' => $menu->getId()));
        }

        return $this->render('RestaurantBundle:menu:edit.html.twig', array(
            'menu' => $menu,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Deletes a menu entity.
     *
     * @Route("/delete/{id}", name="menu_delete")
     * @Method("GET")
     * @Security("has_role('ROLE_CHEF')")
     */
    public function deleteAction(Menu $menu)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($menu);
        $em->flush($menu);

        return $this->redirectToRoute('menu_admin_index');
    }


    /**
     * Creates a form to delete a menu entity.
     *
     * @param Menu $menu The menu entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Menu $menu)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('menu_delete', array('id' => $menu->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Create a form to filter menus
     * @return mixed
     */
    private function createFormFilters(){

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('menu_admin_index'))
            ->setMethod('POST')
            ->add('filtre',ChoiceType::class, array(
                'choices' => array(
                    'En Attente' => 'PENDING',
                    'Refusé'     => 'REFUSED',
                    'Validé' => 'ACCEPTED',
                    'Brouillon' => 'DRAFT'
                )
            ) )
            ->add('Filter', SubmitType::class, array('label' => 'Filtrer'))
            ->getForm();
    }

    // --------------  MAILS METHOD ----------------- \\

    /**
     * Send Mail To Serveurs
     * @param $menu
     */
    private function sendMailToServeurs($menu)
    {
        $em = $this->getDoctrine()->getManager();

        $listServeur = $em->getRepository('RestaurantBundle:User')->findByRole('ROLE_SERVEUR');

        foreach ($listServeur as $serveur) {
            $message = \Swift_Message::newInstance()
                ->setSubject('Un menu a été mis en ligne')
                ->setFrom('restaurantcodeacademy@gmail.com')
                ->setTo($serveur->getEmail())
                ->setBody(
                    $this->renderView(
                        'RestaurantBundle:Emails:accepted-menu.html.twig',
                        array('firstname' => $serveur->getFirstname(),
                            'lastname' => $serveur->getLastname(),
                            'menu' => $menu)
                    ),
                    'text/html'
                )
            ;
            $this->get('mailer')->send($message);
        }
    }


    /**
     * Send Mail To Chef And Reviewer for added Menu
     * @param $menu
     */
    private function sendMailToChefAndReviewerAdd($menu){
        $em = $this->getDoctrine()->getManager();
        $listChef = $em->getRepository('RestaurantBundle:User')->findByRole('ROLE_CHEF');
        $listReviewer = $em->getRepository('RestaurantBundle:User')->findByRole('ROLE_REVIEWER');
        $listReceivers = array_merge($listChef, $listReviewer);

        foreach ($listReceivers as $receiver) {
            $message = \Swift_Message::newInstance()
                ->setSubject('Un menu a été créé')
                ->setFrom('restaurantcodeacademy@gmail.com')
                ->setTo($receiver->getEmail())
                ->setBody(
                    $this->renderView(
                        'RestaurantBundle:Emails:created-menu.html.twig',
                        array('firstname' => $receiver->getFirstname(),
                            'lastname' => $receiver->getLastname(),
                            'menu' => $menu)
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);
        }
    }


    /**
     * Send Mail To Chef And Reviewer for edited Menu
     * @param $menu
     */
    private function sendMailToChefAndReviewerEdit($menu)
    {
        $em = $this->getDoctrine()->getManager();
        $listChef = $em->getRepository('RestaurantBundle:User')->findByRole('ROLE_CHEF');
        $listReviewer = $em->getRepository('RestaurantBundle:User')->findByRole('ROLE_REVIEWER');
        $listReceivers = array_merge($listChef, $listReviewer);

        foreach ($listReceivers as $receiver) {
            $message = \Swift_Message::newInstance()
                ->setSubject('Un menu a été modifié')
                ->setFrom('restaurantcodeacademy@gmail.com')
                ->setTo($receiver->getEmail())
                ->setBody(
                    $this->renderView(
                        'RestaurantBundle:Emails:edited-menu.html.twig',
                        array('firstname' => $receiver->getFirstname(),
                            'lastname' => $receiver->getLastname(),
                            'menu' => $menu)
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);
        }
    }

    // --------------  / MAILS METHOD ----------------- \\
}
