<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\Allergene;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Allergene controller.
 *
 * @Route("allergene")
 */
class AllergeneController extends Controller
{
    /**
     * Lists all allergene entities.
     *
     * @Route("/index/{page}", name="allergene_index", requirements={"page" = "\d+"},defaults={"page" = 1})
     * @Security("has_role('ROLE_SERVEUR')")
     * @Method("GET")
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();

        $allergenes = $em->getRepository('RestaurantBundle:Allergene')->findAllPagination($page, Allergene::MAX_PER_PAGE);

        $pagination = array(
            'page' => $page,
            'route' => 'allergene_index',
            'pages_count' => ceil(count($allergenes) / Allergene::MAX_PER_PAGE),
            'route_params' => array()
        );
        return $this->render('RestaurantBundle:allergene:index.html.twig', array(
            'allergenes' => $allergenes,
            'pagination' => $pagination
        ));
    }


    /**
     * Creates a new allergene entity.
     * @Security("has_role('ROLE_EDITEUR')")
     * @Route("/new", name="allergene_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $allergene = new Allergene();
        $form = $this->createForm('RestaurantBundle\Form\AllergeneType', $allergene);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($allergene);
            $em->flush($allergene);

            return $this->redirectToRoute('allergene_show', array('id' => $allergene->getId()));
        }

        return $this->render('RestaurantBundle:allergene:new.html.twig', array(
            'allergene' => $allergene,
            'form' => $form->createView(),
        ));
    }


    /**
     * Finds and displays a allergene entity.
     * @Security("has_role('ROLE_SERVEUR')")
     * @Route("/{id}", name="allergene_show")
     * @Method("GET")
     */
    public function showAction(Allergene $allergene)
    {
        $deleteForm = $this->createDeleteForm($allergene);

        return $this->render('RestaurantBundle:allergene:show.html.twig', array(
            'allergene' => $allergene,
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing allergene entity.
     * @Security("has_role('ROLE_EDITEUR')")
     * @Route("/{id}/edit", name="allergene_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Allergene $allergene)
    {
        $deleteForm = $this->createDeleteForm($allergene);
        $editForm = $this->createForm('RestaurantBundle\Form\AllergeneType', $allergene);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('allergene_edit', array('id' => $allergene->getId()));
        }

        return $this->render('RestaurantBundle:allergene:edit.html.twig', array(
            'allergene' => $allergene,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Deletes a allergene entity.
     *
     * @Route("/delete/{id}", name="allergene_delete")
     * @Method("GET")
     * @Security("has_role('ROLE_CHEF')")
     */
    public function deleteAction(Allergene $allergene)
    {
            $em = $this->getDoctrine()->getManager();
            $em->remove($allergene);
            $em->flush($allergene);

        return $this->redirectToRoute('allergene_index');
    }


    /**
     * Creates a form to delete a allergene entity.
     *
     * @param Allergene $allergene The allergene entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Allergene $allergene)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('allergene_delete', array('id' => $allergene->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
