<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{

    /**
     * Generate user entities.
     *
     * @Route("/loadUsers", name="load_users")
     *
     * @Method("GET")
     */
    public function addAdminAction(){
        $editeur = new User();
        $editeur
            ->setLogin('editeur')
            ->setPassword('$2y$12$g6SZY3bqve7WQlEm20jroePkTFEyGa1C6RHfKxN27V8iWMlJrWoi.')
            ->setFirstname('editeur')
            ->setLastname('editeur')
            ->setEmail('editeur@gmail.com')
            ->setRoles(array('ROLE_EDITEUR'));
        $this->getDoctrine()->getManager()->persist($editeur);

        $serveur = new User();
        $serveur
            ->setLogin('serveur')
            ->setPassword('$2y$12$g6SZY3bqve7WQlEm20jroePkTFEyGa1C6RHfKxN27V8iWMlJrWoi.')
            ->setFirstname('serveur')
            ->setLastname('serveur')
            ->setEmail('serveur@gmail.com')
            ->setRoles(array('ROLE_SERVEUR'));
        $this->getDoctrine()->getManager()->persist($serveur);

        $reviewer = new User();
        $reviewer
            ->setLogin('reviewer')
            ->setPassword('$2y$12$g6SZY3bqve7WQlEm20jroePkTFEyGa1C6RHfKxN27V8iWMlJrWoi.')
            ->setFirstname('reviewer')
            ->setLastname('reviewer')
            ->setEmail('reviewer@gmail.com')
            ->setRoles(array('ROLE_REVIEWER'));
        $this->getDoctrine()->getManager()->persist($reviewer);

        $chef = new User();
        $chef
            ->setLogin('chef')
            ->setPassword('$2y$12$g6SZY3bqve7WQlEm20jroePkTFEyGa1C6RHfKxN27V8iWMlJrWoi.')
            ->setFirstname('chef')
            ->setLastname('chef')
            ->setEmail('chef@gmail.com')
            ->setRoles(array('ROLE_CHEF'));
        $this->getDoctrine()->getManager()->persist($chef);

        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('home');
    }


    /**
     * Lists all user entities.
     *
     * @Route("/{page}", name="admin_user_index",requirements={"page" = "\d+"},defaults={"page" = 1})
     * @Security("has_role('ROLE_CHEF')")
     * @Method("GET")
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('RestaurantBundle:User')->findAllPagination($page, User::MAX_PER_PAGE);

        $pagination = array(
            'page' => $page,
            'route' => 'admin_user_index',
            'pages_count' => ceil(count($users) / User::MAX_PER_PAGE),
            'route_params' => array()
        );

        return $this->render('RestaurantBundle:user:index.html.twig', array(
            'users' => $users,
            'pagination' => $pagination
        ));
    }


    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="user_new")
     * @Security("has_role('ROLE_CHEF')")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('RestaurantBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $em->persist($user);
            $em->flush($user);

            $this->addFlash('success', 'L\'utilisateur a bien été créé');
            return $this->redirectToRoute('user_show', array('id' => $user->getId()));
        }

        return $this->render('RestaurantBundle:user:new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }


    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}", name="user_show")
     * @Security("has_role('ROLE_CHEF')")
     * @Method("GET")
     */
    public function showAction(User $user)
    {
        $deleteForm = $this->createDeleteForm($user);

        return $this->render('RestaurantBundle:user:show.html.twig', array(
            'user' => $user,
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Security("has_role('ROLE_CHEF')")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('RestaurantBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_edit', array('id' => $user->getId()));
        }

        return $this->render('RestaurantBundle:user:edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Deletes a user entity.
     *
     * @Route("/{id}", name="user_delete")
     * @Security("has_role('ROLE_CHEF')")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush($user);
        }

        return $this->redirectToRoute('admin_user_index');
    }


    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     * @Security("has_role('ROLE_CHEF')")
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
