<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\Reservation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;


/**
 * Reservation controller.
 *
 * @Route("reservation")
 */
class ReservationController extends Controller
{

    /**
     * Lists all reservation entities.
     *
     * @Route("/index/{page}", name="reservation_index",requirements={"page" = "\d+"},defaults={"page" = 1})
     * @Security("has_role('ROLE_SERVEUR')")
     * @Method("GET")
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();

        $reservations = $em->getRepository('RestaurantBundle:Reservation')->findAllPagination($page,Reservation::MAX_PER_PAGE);

        $pagination = array(
            'page' => $page,
            'route' => 'reservation_index',
            'pages_count' => ceil(count($reservations) / Reservation::MAX_PER_PAGE),
            'route_params' => array()
        );

        return $this->render('RestaurantBundle:reservation:index.html.twig', array(
            'reservations' => $reservations,
            'pagination' => $pagination
        ));
    }


    /**
     * Creates a new reservation entity.
     *
     * @Route("/", name="reservation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $reservation = new Reservation();
        $form = $this->createForm('RestaurantBundle\Form\ReservationType', $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $nbPersonne = $em->getRepository('RestaurantBundle:Reservation')->getNBPeopleForThisDay($reservation->getReservationDate()) + $reservation->getPeopleNumber();

            if(Reservation::NB_PERSONNES_MAX - $nbPersonne >= 0) {
                $em->persist($reservation);
                $em->flush($reservation);
                $request->getSession()->getFlashbag()->add('success', 'La réservation a été prise en compte, vous recevrez mail quand elle aura été validée');

                $this->sendMailToAll($reservation);

                return $this->redirectToRoute('home');

            }else{
                $request->getSession()->getFlashbag()->add('error', 'Nombre de places demandé indisponible pour le jour indiqué, veuillez changer la date');
                return $this->render('RestaurantBundle:reservation:new.html.twig', array(
                    'reservation' => $reservation,
                    'form' => $form->createView(), ));
            }
        }

        return $this->render('RestaurantBundle:reservation:new.html.twig', array(
            'reservation' => $reservation,
            'form' => $form->createView(),
        ));
    }


    /**
     * Validate a reservation
     *
     * @param Reservation $reservation
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/validation/{id}", name="reservation_validate")
     * @Security("has_role('ROLE_SERVEUR')")
     * @Method("GET")
     */
    public function acceptReservationAction(Reservation $reservation)
    {
        $reservation->setIsConfirmed(true);
        $this->getDoctrine()->getManager()->flush();
        $this->sendMailToClientAccepted($reservation);

        return $this->redirectToRoute('reservation_index');
    }


    /**
     * Deletes a reservation entity.
     *
     * @Route("/delete/{id}", name="reservation_delete")
     * @Method("GET")
     * @Security("has_role('ROLE_SERVEUR')")
     */
    public function deleteAction(Reservation $reservation)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($reservation);
        $em->flush($reservation);
        $this->sendMailToClientRefused($reservation);

        return $this->redirectToRoute('reservation_index');
    }


    // --------------  MAILS METHOD ----------------- \\

    /**
     * Send Mail To Users
     * @param $reservation
     */
    private function  sendMailToAll($reservation)
    {
        $em = $this->getDoctrine()->getManager();
        $listUser = $em->getRepository('RestaurantBundle:User')->findAll();

        foreach ($listUser as $user) {
            $message = \Swift_Message::newInstance()
                ->setSubject('Une réservation a été créée')
                ->setFrom('restaurantcodeacademy@gmail.com')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(
                        'RestaurantBundle:Emails:created-reservation.html.twig',
                        array('firstname' => $user->getFirstname(),
                            'lastname' => $user->getLastname(),
                            'reservation' => $reservation)
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);
        }
    }


    /**
     * Send Mail To Notify Client For Accepted Reservation
     * @param $reservation
     */
    private function sendMailToClientAccepted($reservation)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Votre réservation a été validée')
            ->setFrom('restaurantcodeacademy@gmail.com')
            ->setTo($reservation->getEmail())
            ->setBody(
                $this->renderView(
                    'RestaurantBundle:Emails:accepted-reservation.html.twig',
                    array('reservation' => $reservation)
                ),
                'text/html'
            )
        ;
        $this->get('mailer')->send($message);
    }


    /**
     * Send Mail To Notify Client For Refused Reservation
     * @param $reservation
     */
    private function sendMailToClientRefused($reservation)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Votre réservation a été refusée')
            ->setFrom('restaurantcodeacademy@gmail.com')
            ->setTo($reservation->getEmail())
            ->setBody(
                $this->renderView(
                    'RestaurantBundle:Emails:refused-reservation.html.twig',
                    array('reservation' => $reservation)
                ),
                'text/html'
            )
        ;
        $this->get('mailer')->send($message);
    }

    // --------------  / MAILS METHOD ----------------- \\
}
