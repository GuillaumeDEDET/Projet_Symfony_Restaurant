<?php

namespace RestaurantBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class ReservationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('reservationDate', DateTimeType::class, array(
                'date_format' => 'yyyy-MM-dd HH:mm',
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => 'Date de réservation'
        ))
            ->add('peopleNumber', null,array('label' => 'Nombre de personnes'))
            ->add('email', null, array('label' => 'Votre email'))
            ->add('lastname', null, array('label' => 'Votre nom de famille'))
            ->add('firstname', null, array('label' => 'Votre prénom'))
            ->add('telNumber', null, array('label' => 'Votre numéro de téléphone'));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RestaurantBundle\Entity\Reservation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'restaurantbundle_reservation';
    }


}
