<?php

namespace RestaurantBundle\Form;

use RestaurantBundle\Entity\Allergene;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class PlatType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', null, array('label' => 'Nom du plat'))
            ->add('description')
            ->add('price' , null, array('label' => 'Prix'))
            ->add('isHomeMade', null, array('label' => 'Fait maison ?'))
            ->add('image', FileType::class, array(
                'data_class' => null,
                'required' => false,
                'label' => 'Photo du plat'
            ))
            ->add('allergenes', EntityType::class, array(
                'class' => Allergene::class,
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true,
                'required' => false,
                'by_reference' => false,
            ))
            ->add('category', ChoiceType::class, array(
                'choices' => array(
                    'Entrée' => 'Entrée',
                    'Plat'   => 'Plat',
                    'Dessert'=> 'Dessert'
                ),
                'label' => 'Catégorie'
            ))
            ->add('saveAsDraft', CheckboxType::class, array(
                'label'    => 'Enregistrer en tant que brouillon ?',
                'required' => false,
                'mapped' => false
            ));
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RestaurantBundle\Entity\Plat'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'restaurantbundle_plat';
    }


}
