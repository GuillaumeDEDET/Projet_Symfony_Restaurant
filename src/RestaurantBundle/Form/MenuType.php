<?php

namespace RestaurantBundle\Form;

use RestaurantBundle\Entity\Plat;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use RestaurantBundle\Repository\PlatRepository;


class MenuType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', null,array('label' => 'Nom du menu'))
            ->add('price', null,array('label' => 'Prix'))
            ->add('plats', EntityType::class, array(
                'class' => Plat::class,
                'query_builder' => function (PlatRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->where("p.status = 'ACCEPTED'");
                    },
                'choice_label' => 'title',
                'expanded' => true,
                'multiple' => true,
                'by_reference' => false
            ))
            ->add('showOrder', null ,array('label' => 'Ordre d\'apparition'))
            ->add('saveAsDraft', CheckboxType::class, array(
                'label'    => 'Enregistrer en tant que brouillon ?',
                'required' => false,
                'mapped' => false
            ));    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RestaurantBundle\Entity\Menu'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'restaurantbundle_menu';
    }


}
