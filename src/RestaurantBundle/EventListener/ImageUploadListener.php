<?php
/**
 * Created by PhpStorm.
 * User: quentin
 * Date: 03/02/2017
 * Time: 10:15
 */
namespace RestaurantBundle\EventListener;

use RestaurantBundle\Entity\Plat;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use RestaurantBundle\Services\FileUploader;

/**
 * Class ImageUploadListener
 */
class ImageUploadListener
{
    private $uploader;

    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    /**
     * Upload File
     *
     * @param $entity
     */
    private function uploadFile($entity)
    {
        // upload only works for Plat entities
        if (!$entity instanceof Plat) {
            return;
        }

        $file = $entity->getImage();

        // only upload new files
        if (!$file instanceof UploadedFile) {
            return;
        }

        $fileName = $this->uploader->upload($file);
        $entity->setImage($fileName);
    }
}
