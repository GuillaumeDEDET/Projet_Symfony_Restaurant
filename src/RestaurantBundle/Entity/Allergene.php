<?php

namespace RestaurantBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Allergene
 *
 * @ORM\Table(name="allergene")
 * @ORM\Entity(repositoryClass="RestaurantBundle\Repository\AllergeneRepository")
 * @ORM\HasLifecycleCallbacks()

 */
class Allergene
{
    const MAX_PER_PAGE = 5;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="Plat", mappedBy="allergenes")
     *
     */
    private $plats;

    /**
     * Allergene constructor.
     *
     */
    public function __construct()
    {
        $this->plats = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Allergene
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getPlats()
    {
        return $this->plats;
    }

    /**
     * @param Plat $plat
     * @return $this
     */
    public function addPlat(Plat $plat){
        $this->plats->add($plat);
        return $this;
    }

    public function setPlats($plats){
        foreach($plats as $plat){
            $this->addPlat($plat);
        }

        return $this;
    }

    public function removePlat($plat){
        $this->plats->removeElement($plat);
    }
}

