<?php

namespace RestaurantBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Plat
 *
 * @ORM\Table(name="plat")
 * @ORM\Entity(repositoryClass="RestaurantBundle\Repository\PlatRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Plat
{

    const MAX_PER_PAGE = 5;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255, unique=true)
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var float
     * @Assert\NotBlank()
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var string
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;

    /**
     * @var bool
     *
     * @ORM\Column(name="isHomeMade", type="boolean")
     */
    private $isHomeMade;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="category", type="string", length=255)
     */
    private $category;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    private $updatedAt;

    /**
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="plats")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $author;

    /**
     * @ORM\ManyToMany(targetEntity="Menu", mappedBy="plats")
     */
    private $menus;

    /**
     * @ORM\ManyToMany(targetEntity="Allergene", inversedBy="plats", cascade={"persist"})
     * @ORM\JoinTable(name="plats_allergenes")
     */
    private $allergenes;

    /**
     * Plat constructor.
     *
     */
    public function __construct()
    {
        $this->menus = new ArrayCollection();
        $this->allergenes = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Plat
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Plat
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Plat
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Plat
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set isHomeMade
     *
     * @param boolean $isHomeMade
     *
     * @return Plat
     */
    public function setIsHomeMade($isHomeMade)
    {
        $this->isHomeMade = $isHomeMade;

        return $this;
    }

    /**
     * Get isHomeMade
     *
     * @return bool
     */
    public function getIsHomeMade()
    {
        return $this->isHomeMade;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Plat
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }


    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     *
     * @return string
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this->category;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     * @return $this
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @param Menu $menu
     * @return $this
     */
    public function addMenu(Menu $menu){
        $this->menus->add($menu);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMenus(){
        return $this->menus;
    }

    public function setMenus($menus){
        foreach($menus as $menu){
            $this->addMenu($menu);
        }

        return $this;
    }

    public function removeMenu($menu){
        $this->menus->removeElement($menu);
    }

    /**
     * @return ArrayCollection
     */
    public function getAllergenes()
    {
        return $this->allergenes;
    }

    /**
     * @param Allergene $allergene
     * @return Plat
     */
    public function addAllergene(Allergene $allergene)
    {
        $allergene->addPlat($this);
        $this->allergenes->add($allergene);

        return $this;
    }

    /**
     * @param $allergenes
     * @return $this
     */
    public function setAllergenes($allergenes){
        foreach($allergenes as $allergene){
            $this->addAllergene($allergene);
        }

        return $this;
    }


    public function removeAllergene(Allergene $allergene){
        $allergene->removePlat($this);
        $this->allergenes->removeElement($allergene);
    }
    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }


    /**
     * Set updatedAt
     *
     * @param \DateTIme $updatedAt
     *
     * @return Plat
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist(){
        $this->setCreatedAt(new \DateTime());
        $this->preUpdate();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        $this->setUpdatedAt(new \DateTime());
    }
}

