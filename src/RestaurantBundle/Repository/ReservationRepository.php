<?php

namespace RestaurantBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * ReservationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ReservationRepository extends \Doctrine\ORM\EntityRepository
{


    public function getNBPeopleForThisDay(\DateTime $date){

        $beginningOfTheDay = new \DateTime($date->format("Y-m-d")."00:00:00");
        $endOfTheDay = new \DateTime($date->format("Y-m-d")."23:59:59");
        return $this
            ->createQueryBuilder('r')
            ->andWhere('r.reservationDate BETWEEN :beginning and :end')
            ->andWhere('r.isConfirmed = true')
            ->select('SUM(r.peopleNumber)')
            ->setParameter('beginning', $beginningOfTheDay)
            ->setParameter('end', $endOfTheDay)
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    public function getAllUnconfirmed(){
        return $this->createQueryBuilder('r')
            ->where('r.isConfirmed = false')
            ->select('r')
            ->getQuery()
            ->getArrayResult();
    }


    public function findAllPagination($page, $maxPerPage = 10){
        $q = $this->createQueryBuilder('r')
            ->setFirstResult(($page-1)*$maxPerPage)
            ->setMaxResults($maxPerPage);

        return new Paginator($q);
    }
}
