<?php
/**
 * Created by PhpStorm.
 * User: guillaume
 * Date: 07/02/2017
 * Time: 11:01
 */

namespace RestaurantBundle\Services;

use Symfony\Component\Templating\EngineInterface;

class Mailer
{
    protected $mailer;
    protected $templating;
    private $from = "restaurantcodeacademy@gmail.com";
    private $reply = "restaurantcodeacademy@gmail.com";
    private $name = "Equipe Restaurant Bundle";

    public function __construct($mailer, EngineInterface $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    protected function sendMessage($to, $subject, $body)
    {
        $mail = \Swift_Message::newInstance();

        $mail
            ->setFrom($this->from,$this->name)
            ->setTo($to)
            ->setSubject($subject)
            ->setBody($body)
            ->setReplyTo($this->reply,$this->name)
            ->setContentType('text/html');

        $this->mailer->send($mail);
    }

    public function sendNewPlatMessage(\RestaurantBundle\Entity\User $user){
        $subject = "Un nouveau plat a été ajouté";
        $template = 'RestaurantBundle:Email:new-plat.html.twig';
        $to = $user->getEmail();
        $body = $this->templating->render($template, array('user' => $user));
        $this->sendMessage($to, $subject, $body);
    }
}